# CHANGELOG

# 0.2.0

* feat: add `var.execution_role_extras_policies` to manage execution role policies
* feat: `var.properties` now expect a map instead of jsonencode(map)
* chore: add tfsec to pre-commit

# 0.1.0

* feat: initial release
